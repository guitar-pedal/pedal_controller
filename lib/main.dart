// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/login_signup/login_signup_view.dart';
import 'package:pedal_controller/app/utils/router.dart';
import 'package:pedal_controller/app/utils/themes.dart';
import 'package:pedal_controller/data/constants.dart' as Constants;
import 'package:pedal_controller/amplifyconfiguration.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final CustomRouter _router;
  bool _amplifyConfigured = false;
  Amplify _amplifyInstance = Amplify();

  _MyAppState() : _router = CustomRouter() {
    initLogger();
  }

  @override
  void initState() {
    super.initState();
    _configureAmplify();
  }

  Widget _display() {
    return LoginSignUpPage();
  }

  @override
  Widget build(BuildContext context) {
    FlutterCleanArchitecture.debugModeOn();
    return MaterialApp(
      title: Constants.PROJECT_TITLE,
      theme: defaultTheme,
      darkTheme: ThemeData.dark(),
      home: _display(),
      onGenerateRoute: _router.getRoute,
      navigatorObservers: [_router.routeObserver],
    );
  }

  void initLogger() {
    Logger.root.level = Level.ALL;
    Logger.root.onRecord.listen((record) {
      dynamic e = record.error;
      String m = e.toString();
      print(
          '${record.loggerName}: ${record.level.name}: ${record.message} ${m != 'null' ? m : ''}');
    });
    Logger.root.info("Logger initialized.");
  }

  void _configureAmplify() async {
    if (!mounted) return;

    // Add Cognito Plugins
    AmplifyAuthCognito authPlugin = AmplifyAuthCognito();
    _amplifyInstance.addPlugin(authPlugins: [authPlugin]);

    // Once Plugins are added, configure Amplify
    await _amplifyInstance.configure(amplifyconfig);
    try {
      setState(() {
        _amplifyConfigured = true;
      });
    } catch (e) {
      print(e);
    }
  }
}


