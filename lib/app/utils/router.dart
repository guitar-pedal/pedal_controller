import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pedal_controller/app/pages/pages.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';
import 'package:pedal_controller/domain/entities/channel_info.dart';
import 'package:pedal_controller/domain/entities/effect_info.dart';

class CustomRouter {
  final RouteObserver<PageRoute> routeObserver;

  CustomRouter() : routeObserver = RouteObserver<PageRoute>();

  Route<dynamic> getRoute(RouteSettings settings) {
    switch (settings.name) {
      case Pages.board:
        return _buildRoute(settings, BoardPage());
      case Pages.loginSignup:
        return _buildRoute(settings, LoginSignUpPage());
      case Pages.bankPage:
        final BankInfo args = settings.arguments;
        return _buildRoute(settings, BankPage(info: args));
      case Pages.channelPage:
        final ChannelInfo args = settings.arguments;
        return _buildRoute(settings, ChannelPage(info: args));
      // TODO: Uncomment the lines below when the clean architecture is complete.
       case Pages.effectPage:
         final EffectInfo args = settings.arguments;
         return _buildRoute(settings, EffectPage(info: args));
      default:
        return null;
    }
  }

  MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
    return new MaterialPageRoute(
      settings: settings,
      builder: (ctx) => builder,
    );
  }
}
