import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const Color logoBlue = Color(0xff4adece);
const Color logoInverted = Color(0xffbe2334);

ThemeData defaultTheme = ThemeData(
    primaryColor: logoBlue,
    accentColor: logoInverted,
    brightness: Brightness.light);
