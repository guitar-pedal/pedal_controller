import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_channels_usecase.dart';

class BankPagePresenter extends Presenter {
  Function getChannelsOnNext;
  Function getChannelsOnComplete;
  Function getChannelsOnError;

  final GetChannelsUseCase _getChannelsUseCase;

  BankPagePresenter(channelRepo, authRepository)
      : _getChannelsUseCase = GetChannelsUseCase(channelRepo);

  void getBankChannels({@required User user}) {
    _getChannelsUseCase.execute(
        _GetChannelObserver(this), GetChannelsUseCaseParams(user, 'test'));
  }

  @override
  void dispose() {
    _getChannelsUseCase.dispose();
  }
}

class _GetChannelObserver extends Observer<GetChannelsUseCaseResponse> {
  final BankPagePresenter _presenter;

  _GetChannelObserver(this._presenter);

  @override
  void onNext(response) {
    this._presenter.getChannelsOnNext(response.channels);
  }

  @override
  void onComplete() {
    this._presenter.getChannelsOnComplete();
  }

  @override
  void onError(e) {
    this._presenter.getChannelsOnError(e);
  }
}