import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/bank_page/bank_page_controller.dart';
import 'package:pedal_controller/app/widgets/channel_card.dart';
import 'package:pedal_controller/app/widgets/drawer/app_drawer.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_auth_repository.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_channel_repository.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';

/// Creates a page that lists the channels that make up a bank.

class BankPage extends View {
  BankPage({Key key, @required this.info}) : super(key: key);
  final BankInfo info;

  @override
  BankPageView createState() => BankPageView(this.info);
}

class BankPageView extends ViewState<BankPage, BankPageController> {
  BankPageView(this.info)
      : super(BankPageController(
          DummyChannelRepository(),
          DummyAuthRepository(),
          info,
        ));

  final BankInfo info;

  @override
  Widget get view {
    return Container(
      child: Scaffold(
        endDrawer: AppDrawer(),
        appBar: appBar,
        key: globalKey,
        body: ControlledWidgetBuilder<BankPageController>(
          builder: (context, controller) {
            List<ChannelCard> cards = controller.channels
                .map((channel) => ChannelCard(channel))
                .toList();
            return ListView(scrollDirection: Axis.vertical, children: cards);
          },
        ),
      ),
    );
  }

  AppBar get appBar => AppBar(
        backgroundColor: this.info.color,
        title: Text("${this.info.number.toString()} | ${this.info.name}"),
        leading: IconButton(
          icon: Icon(Icons.keyboard_arrow_left),
          onPressed: () => Navigator.pop(context),
        ),
      );
}
