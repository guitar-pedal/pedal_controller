import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/bank_page/bank_page_presenter.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';
import 'package:pedal_controller/domain/entities/channel_info.dart';

class BankPageController extends Controller {
  BankPagePresenter _presenter;
  bool _channelsRetrieved;
  bool _isLoading;
  final BankInfo _bankInfo;
  List<String> channels = [];
  final logger;

  BankPageController(channelRepository, authRepository, bankInfo)
      : _presenter = BankPagePresenter(channelRepository, authRepository),
        _bankInfo = bankInfo,
        logger = Logger("BankPageController") {
    _channelsRetrieved = false;
    _isLoading = true;
    retrieveData();
  }

  void retrieveData() {
    logger.fine("Retrieving data");
    _presenter.getBankChannels(user: null);
  }

  /// Function called when a list item is tapped to navigate to a new screen
  void listOnTap(String name) {
    Navigator.of(getContext())
        .pushNamed('/channel', arguments: ChannelInfo(name, this._bankInfo));
  }

  @override
  void initListeners() {
    _presenter.getChannelsOnNext = (List<String> channels) {
      this.channels = channels;
    };

    _presenter.getChannelsOnComplete = () {
      this._channelsRetrieved = true;
      dismissLoading();
    };

    _presenter.getChannelsOnError = (e) {
      dismissLoading();
      logger.severe(e);
    };
  }

  void dismissLoading() {
    _isLoading = false;
    refreshUI();
  }

  @override
  void didPopNext() {
    retrieveData();
    super.didPopNext();
  }
}
