import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/widgets/drawer/app_drawer.dart';
import 'package:pedal_controller/app/widgets/effect_edit/effect_edit_card_factory.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_auth_repository.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_effect_edit_info_repository.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/effect_info.dart';

import 'effect_page_controller.dart';

class EffectPage extends View {
  final EffectInfo info;
  EffectPage({Key key, @required this.info}) : super(key: key);

  @override
  State createState() => EffectPageView(this.info);
}

class EffectPageView extends ViewState<EffectPage, EffectPageController> {
  EffectPageView(this.info)
      : super(EffectPageController(
            DummyAuthRepository(), DummyEffectEditInfoRepository()));

  final EffectInfo info;

  @override
  Widget get view {
    return Container(
      child: Scaffold(
        endDrawer: AppDrawer(),
        appBar: appBar,
        key: globalKey,
        body: ControlledWidgetBuilder<EffectPageController>(
            builder: (context, controller) {
          List<Widget> cards = [];
          for (final inf in controller.effectEditInfo) {
            cards.add(_buildListItemWithKey(inf));
          }
          return Container(
            child: ListView(scrollDirection: Axis.vertical, children: cards),
          );
        }),
      ),
    );
  }

  AppBar get appBar => AppBar(
        // TODO: Populate from args
        title: Text(
            "${this.info.channelInfo.bankInfo.number.toString()} | ${this.info.channelInfo.bankInfo.name} | ${this.info.channelInfo.name} | ${this.info.name}"),
        backgroundColor: this.info.channelInfo.bankInfo.color,
        leading: IconButton(
          icon: Icon(Icons.keyboard_arrow_left),
          onPressed: () => Navigator.pop(context),
        ),
      );

  Container _buildListItemWithKey(EffectEditInfo info) {
    return Container(
        key: ValueKey(info),
        child: EffectEditCardFactory.createEffectEditCard(info));
  }
}
