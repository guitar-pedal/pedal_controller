import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/effect_page/effect_page_presenter.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';

class EffectPageController extends Controller {
  EffectPagePresenter _presenter;
  bool _infoRetrieved;
  bool _isLoading;
  List<EffectEditInfo> effectEditInfo = [];
  final logger;

  EffectPageController(authRepo, effectRepo)
      : _presenter = EffectPagePresenter(authRepo, effectRepo),
        logger = Logger("EffectPageController") {
    _infoRetrieved = false;
    _isLoading = true;
    retrieveData();
  }

  void retrieveData() {
    _presenter.getEffects(user: null);
  }

  void setData(EffectEditInfo info) {
    _presenter.setEffect(effectEditInfo: info);
  }

  @override
  void initListeners() {
    /// The Get callbacks
    _presenter.getDataOnNext = (List<EffectEditInfo> info) {
      this.effectEditInfo = info;
    };

    _presenter.getDataOnError = (e) {
      dismissLoading();
      logger.severe(e);
    };

    _presenter.getDataOnComplete = () {
      this._infoRetrieved = true;
      dismissLoading();
    };

    /// The Set Callbacks
    _presenter.setDataOnComplete = () {
      dismissLoading();
    };

    _presenter.setDataOnNext = (EffectEditInfo info) {
      // TODO: Possibly update the list item? Probably not though
      dismissLoading();
    };

    _presenter.setDataOnError = (e) {
      dismissLoading();
      logger.severe(e);
    };
  }

  void dismissLoading() {
    _isLoading = false;
    refreshUI();
  }

  @override
  void didPopNext() {
    retrieveData();
    super.didPopNext();
  }
}
