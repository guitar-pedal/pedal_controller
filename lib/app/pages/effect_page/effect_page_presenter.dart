import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_effect_edit_info_usecase.dart';
import 'package:pedal_controller/domain/usecases/set_effect_edit_info_usecase.dart';

class EffectPagePresenter extends Presenter {
  // Retrieve list of data that make up effect
  Function getDataOnNext;
  Function getDataOnComplete;
  Function getDataOnError;

  // Persist data for a certain item
  Function setDataOnNext;
  Function setDataOnComplete;
  Function setDataOnError;

  final GetEffectEditInfoUseCase _getEffectEditInfoUseCase;
  final SetEffectEditInfoUseCase _setEffectEditInfoUseCase;

  EffectPagePresenter(authRepo, effectRepo)
      : _getEffectEditInfoUseCase = GetEffectEditInfoUseCase(effectRepo),
        _setEffectEditInfoUseCase = SetEffectEditInfoUseCase(effectRepo);

  void getEffects({@required User user}) {
    _getEffectEditInfoUseCase.execute(_GetEffectEditInfoObserver(this),
        GetEffectEditInfoUseCaseParams(user, 'test'));
  }

  void setEffect({@required EffectEditInfo effectEditInfo}) {
    _setEffectEditInfoUseCase.execute(
      _SetEffectEditInfoObserver(this),
      SetEffectEditInfoUseCaseParams(null, effectEditInfo),
    );
  }

  // TODO: Function to execute SET usecase
  @override
  void dispose() {
    _getEffectEditInfoUseCase.dispose();
    // TODO: dispose of set usecase
  }
}

// TODO: GetData usecase observer
class _GetEffectEditInfoObserver
    extends Observer<GetEffectEditInfoUseCaseResponse> {
  final EffectPagePresenter _presenter;

  _GetEffectEditInfoObserver(this._presenter);

  @override
  void onNext(response) {
    this._presenter.getDataOnNext(response.effectEditInfo);
  }

  @override
  void onError(e) {
    this._presenter.getDataOnError(e);
  }

  @override
  void onComplete() {
    this._presenter.getDataOnComplete();
  }
}

// TODO: SetData usecase observer
class _SetEffectEditInfoObserver
    extends Observer<SetEffectEditInfoUseCaseResponse> {
  final EffectPagePresenter _presenter;

  _SetEffectEditInfoObserver(this._presenter);

  @override
  void onError(e) {
    _presenter.setDataOnError(e);
  }

  @override
  void onComplete() {
    _presenter.setDataOnComplete();
  }

  @override
  void onNext(SetEffectEditInfoUseCaseResponse response) {
    _presenter.setDataOnNext(response.effectEditInfo);
  }
}
