import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_effect_info_usecase.dart';

class ChannelPagePresenter extends Presenter {
  Function getChannelInfoOnNext;
  Function getChannelInfoOnComplete;
  Function getChannelInfoOnError;

  final GetEffectInfoUseCase _getChannelInfoUseCase;

  ChannelPagePresenter(effectInfoRepo, authRepo)
      : _getChannelInfoUseCase = GetEffectInfoUseCase(effectInfoRepo);

  void getChannelInfo({@required User user}) {
    // TODO:
    _getChannelInfoUseCase.execute(
        _GetChannelInfoObserver(this), GetEffectInfoUseCaseParams(user, 'test'));
  }

  @override
  void dispose() {
    _getChannelInfoUseCase.dispose();
  }
}

class _GetChannelInfoObserver extends Observer<GetEffectInfoUseCaseResponse> {
  final ChannelPagePresenter _presenter;
  _GetChannelInfoObserver(this._presenter);

  @override
  void onNext(response) {
    this._presenter.getChannelInfoOnNext(response.effects);
  }

  @override
  void onComplete() {
    this._presenter.getChannelInfoOnComplete();
  }

  @override
  void onError(e) {
    this._presenter.getChannelInfoOnError(e);
  }
}
