import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/channel_page/channel_page_controller.dart';
import 'package:pedal_controller/app/widgets/drawer/app_drawer.dart';
import 'package:pedal_controller/app/widgets/effect_card.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_auth_repository.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_effect_repository.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';
import 'package:pedal_controller/domain/entities/channel_info.dart';

/// Creates a page that lists the effects that make up an channel.
/// It is necessary to pass in [ChannelInfo] into the constructor,
/// which contains the name of the channel, along with the [BankInfo]
/// which contains the channel.
class ChannelPage extends View {
  ChannelPage({Key key, @required this.info}) : super(key: key);
  final ChannelInfo info;
  @override
  ChannelPageView createState() => ChannelPageView(this.info);
}

class ChannelPageView extends ViewState<ChannelPage, ChannelPageController> {
  ChannelPageView(this.info)
      : super(ChannelPageController(
          DummyEffectRepository(),
          DummyAuthRepository(),
          info,
        ));

  final ChannelInfo info;

  @override
  Widget get view {
    return Container(
      child: Scaffold(
        endDrawer: AppDrawer(),
        appBar: appBar,
        key: globalKey,
        body: ControlledWidgetBuilder<ChannelPageController>(
          builder: (context, controller) {
            List<EffectCard> cards =
                controller.effects.map((effect) => EffectCard(effect)).toList();
            return ListView(scrollDirection: Axis.vertical, children: cards);
          },
        ),
      ),
    );
  }

  AppBar get appBar => AppBar(
        backgroundColor: this.info.bankInfo.color,
        title: Text(
            "${this.info.bankInfo.number.toString()} | ${this.info.bankInfo.name} | ${this.info.name}"),
        leading: IconButton(
          icon: Icon(Icons.keyboard_arrow_left),
          onPressed: () => Navigator.pop(context),
        ),
      );
}
