import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/channel_page/channel_page_presenter.dart';
import 'package:pedal_controller/domain/entities/channel_info.dart';
import 'package:pedal_controller/domain/entities/effect_info.dart';

class ChannelPageController extends Controller {
  ChannelPagePresenter _presenter;
  bool _effectsRetrieved;
  bool _isLoading;
  final ChannelInfo _channelInfo;
  List<String> effects = [];
  final logger;

  ChannelPageController(effectInfoRepository, authRepository, channelInfo)
      : _presenter = ChannelPagePresenter(effectInfoRepository, authRepository),
        _channelInfo = channelInfo,
        logger = Logger("ChannelPageController") {
    _effectsRetrieved = false;
    _isLoading = true;
    retrieveData();
  }

  void retrieveData() {
    // TODO: Get user before effect info?
    logger.fine("Retrieving data");
    _presenter.getChannelInfo(user: null);
  }

  void listOnTap(String name) {
    Navigator.of(getContext())
        .pushNamed('/effect', arguments: EffectInfo(name, this._channelInfo));
  }

  @override
  void initListeners() {
    _presenter.getChannelInfoOnNext = (List<String> effects) {
      this.effects = effects;
    };

    _presenter.getChannelInfoOnComplete = () {
      this._effectsRetrieved = true;
      dismissLoading();
    };

    _presenter.getChannelInfoOnError = (e) {
      dismissLoading();
      logger.severe(e);
    };
  }

  void dismissLoading() {
    _isLoading = false;
    refreshUI();
  }

  @override
  void didPopNext() {
    retrieveData();
    super.didPopNext();
  }
}
