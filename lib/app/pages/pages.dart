export 'package:pedal_controller/app/pages/bank_page/bank_page_view.dart';
export 'package:pedal_controller/app/pages/effect_page/effect_page_view.dart';
export 'package:pedal_controller/app/pages/board_page/board_page_view.dart';
export 'package:pedal_controller/app/pages/channel_page/channel_page_view.dart';
export 'package:pedal_controller/app/pages/login_signup/login_signup_view.dart';

class Pages {
  static const String board = '/board';
  static const String loginSignup = '/loginSignup';
  static const String bankPage = '/bank';
  static const String channelPage = '/channel';
  static const String effectPage = '/effect';
}
