import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/confirm_signup_info.dart';
import 'package:pedal_controller/domain/repositories/auth_repository_impl.dart';

import 'login_signup_controller.dart';

class ConfirmSignupPage extends View {
  @override
  State<StatefulWidget> createState()  => ConfirmSignupState();
}

class ConfirmSignupState extends ViewState<ConfirmSignupPage, LoginSignUpController> {
  TextEditingController userController = TextEditingController();
  TextEditingController codeController = TextEditingController();

  ConfirmSignupState() : super(LoginSignUpController(AuthRepositoryImpl()));

  String get user => userController.text.trim();
  String get code => codeController.text.trim();

  @override
  void initViewState(LoginSignUpController controller) {
    super.initViewState(controller);
  }

  @override
  Widget get view {
    return ControlledWidgetBuilder<LoginSignUpController>(
        builder: (context, controller) {
          return Scaffold(
              key: globalKey,
              body: ListView(
                  padding: EdgeInsets.only(top: 100, left: 15, right: 15),
                  children: <Widget>[
                    Center(
                      child: Column(
                          children: [
                            const Padding(padding: EdgeInsets.all(5.0)),
                            Image.asset(
                              'assets/logos/bigDarkLogo.png',
                              filterQuality: FilterQuality.high,
                            ),
                            TextField(
                              controller: userController,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                icon: Icon(Icons.person),
                                labelText: 'Username',
                              ),
                            ),
                            TextField(
                                controller: codeController,
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  icon: Icon(Icons.confirmation_num),
                                  labelText: 'Code is sent to email',
                                )
                            ),
                            RaisedButton(
                                onPressed: () => controller.confirmSignup(
                                    _getConfirmSignupInfo()),
                                child: const Text('Confirm Sign Up')
                            ),
                          ]
                      ),
                    )
                  ])
          );
        }
    );
  }

  ConfirmSignupInfo _getConfirmSignupInfo() {
    return new ConfirmSignupInfo(username: user, code: code);
  }
}
