import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/domain/entities/confirm_signup_info.dart';
import 'package:pedal_controller/domain/entities/signup_info.dart';
import 'package:pedal_controller/domain/usecases/confirm_signup_usecase.dart';
import 'package:pedal_controller/domain/usecases/login_usecase.dart';
import 'package:pedal_controller/domain/usecases/signup_usecase.dart';

import 'confirm_signup_page.dart';
import 'login_signup_presenter.dart';

class LoginSignUpController extends Controller {
  final LoginSignUpPresenter _loginSignUpPresenter;
  final logger;
  bool isSignedUp;
  bool isLoggedIn;
  bool isSignupConfirm;

  LoginSignUpController(authRepo)
      : _loginSignUpPresenter = LoginSignUpPresenter(authRepo),
        logger = Logger("LoginSignUpController"),
        super() {
    isSignedUp = false;
    isLoggedIn = false;
    isSignupConfirm = false;
  }

  @override
  void initListeners() {
    _loginSignUpPresenter.loginOnComplete = this._loginOnComplete;
    _loginSignUpPresenter.loginOnError = this._loginOnError;
    _loginSignUpPresenter.loginOnNext = this._loginOnNext;
    _loginSignUpPresenter.signUpOnComplete = this._signUpOnComplete;
    _loginSignUpPresenter.signUpOnNext = this._signUpOnNext;
    _loginSignUpPresenter.signUpOnError = this._signUpOnError;
    _loginSignUpPresenter.confirmSignupOnComplete = this._confirmSignupOnComplete;
    _loginSignUpPresenter.confirmSignupOnNext = this._confirmSignupOnNext;
    _loginSignUpPresenter.confrimSignUpOnError = this._confirmSignupOnError;
  }

  //Login
  Future login(LoginData loginData) async {
    _loginSignUpPresenter.login(
        email: loginData.name.trim(), password: loginData.password.trim());
  }

  void _loginOnComplete() {
    if (isLoggedIn) {
      Navigator.of(getContext()).pushReplacementNamed('/board');
    }
  }

  void _loginOnNext(LoginUseCaseResponse response) {
    isLoggedIn = response.loginComplete;
    refreshUI();
  }

    void _loginOnError(e) {
      logger.severe(e);
    }

    //Sign up
    Future signUp(SignupInfo signupInfo) async {
      _loginSignUpPresenter.signUp(
          username: signupInfo.username.trim(),
          email: signupInfo.email.trim(),
          password: signupInfo.password.trim(),
          phone: signupInfo.phone.trim());
    }

    void _signUpOnComplete() {
      if (this.isSignedUp) {
        Navigator.push(getContext(),
            MaterialPageRoute(builder: (context) => ConfirmSignupPage()));
      }
    }

    void _signUpOnNext(SignUpUseCaseResponse response) {
      isSignedUp = response.signUpComplete;
      refreshUI();
    }

    void _signUpOnError(e) {
      logger.severe(e);
    }


    //Confirm Sign up
    Future confirmSignup(ConfirmSignupInfo confirmSignupInfo) {
      _loginSignUpPresenter.confirmSignup(
          username: confirmSignupInfo.username, code: confirmSignupInfo.code);
    }

    void _confirmSignupOnComplete() {
      if (this.isSignupConfirm) {
        Navigator.of(getContext()).pushReplacementNamed('/board');
      }
    }

    void _confirmSignupOnNext(ConfirmSignupUseCaseResponse response) {
      isSignupConfirm = response.confirmSignUpComplete;
      refreshUI();
    }

    void _confirmSignupOnError(e) {
      logger.severe(e);
    }

    Future<String> recoverPassword(String input) async {
      logger.fine("Recover password clicked");
    }
  }