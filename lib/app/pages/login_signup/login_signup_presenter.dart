import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';
import 'package:pedal_controller/domain/usecases/confirm_signup_usecase.dart';
import 'package:pedal_controller/domain/usecases/login_usecase.dart';
import 'package:pedal_controller/domain/usecases/signup_usecase.dart';

class LoginSignUpPresenter extends Presenter {
  AuthRepository _authRepository;
  LoginUseCase _loginUseCase;
  SignUpUseCase _signUpUseCase;
  ConfirmSignupUseCase _confirmSignupUseCase;

  // TODO: Controller callbacks
  Function loginOnComplete;
  Function loginOnError;
  Function loginOnNext;

  Function signUpOnComplete;
  Function signUpOnNext;
  Function signUpOnError;

  Function confirmSignupOnComplete;
  Function confirmSignupOnNext;
  Function confrimSignUpOnError;

  LoginSignUpPresenter(this._authRepository) {
    _loginUseCase = LoginUseCase(_authRepository);
    _signUpUseCase = SignUpUseCase(_authRepository);
    _confirmSignupUseCase = ConfirmSignupUseCase(_authRepository);
  }

  /// Login using the [email] and [password] provided
  void login({@required String email, @required String password}) {
    _loginUseCase.execute(
        _LoginUserCaseObserver(this), LoginUseCaseParams(email, password));
  }

  void signUp({@required String username, @required String email, @required String password, String phone}) {
    _signUpUseCase.execute(
        _RegisterUserCaseObserver(this), SignUpUseCaseParams(username, email, password, phone));
  }

  void confirmSignup({@required String username, @required String code}) {
    _confirmSignupUseCase.execute(
        _ConfirmSignupUseCaseObserver(this), ConfirmSignupUseCaseParams(username, code));
  }

  @override
  void dispose() {
    _loginUseCase.dispose();
    _signUpUseCase.dispose();
    _confirmSignupUseCase.dispose();
  }
}

/// The [Observer] used to observe the `Observable` of the [LoginUseCase]
class _LoginUserCaseObserver implements Observer<void> {

  LoginSignUpPresenter _loginSignupPresenter;

  _LoginUserCaseObserver(this._loginSignupPresenter);

  /// implement if the `Observable` emits a value
  void onNext(response) {
    _loginSignupPresenter.loginOnNext(response);
  }

  /// Login is successful, trigger event in [LoginSignupController]
  void onComplete() {
    // any cleaning or preparation goes here
    _loginSignupPresenter.loginOnComplete();
  }

  /// Login was unsuccessful, trigger event in [LoginSignupController]
  void onError(e) {
    // any cleaning or preparation goes here
    if (_loginSignupPresenter.loginOnError != null) {
      _loginSignupPresenter.loginOnError(e);
    }
  }
}

class _RegisterUserCaseObserver implements Observer<SignUpUseCaseResponse> {
  LoginSignUpPresenter _loginSignupPresenter;

  _RegisterUserCaseObserver(this._loginSignupPresenter);

  void onNext(response) {
    _loginSignupPresenter.signUpOnNext(response);
  }

  void onComplete() {
    _loginSignupPresenter.signUpOnComplete();
  }

  void onError(e) {
    if (_loginSignupPresenter.signUpOnError != null) {
      _loginSignupPresenter.signUpOnError(e);
    }
  }
}

class _ConfirmSignupUseCaseObserver implements Observer<ConfirmSignupUseCaseResponse> {
  LoginSignUpPresenter _loginSignupPresenter;

  _ConfirmSignupUseCaseObserver(this._loginSignupPresenter);

  void onNext(response) {
    _loginSignupPresenter.confirmSignupOnNext(response);
  }

  void onComplete() {
    _loginSignupPresenter.confirmSignupOnComplete();
  }

  void onError(e) {
    if (_loginSignupPresenter.confrimSignUpOnError != null) {
      _loginSignupPresenter.confrimSignUpOnError(e);
    }
  }
}
