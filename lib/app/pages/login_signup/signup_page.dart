import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/login_signup/login_signup_controller.dart';
import 'package:pedal_controller/domain/entities/signup_info.dart';
import 'package:pedal_controller/domain/repositories/auth_repository_impl.dart';

class SignupPage extends View {
  @override
  State<StatefulWidget> createState()  => SignupState();
}

class SignupState extends ViewState<SignupPage, LoginSignUpController> {

  TextEditingController _userController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();

  SignupState() : super(LoginSignUpController(AuthRepositoryImpl()));

  String get _user => _userController.text.trim();
  String get _email => _emailController.text.trim();
  String get _password => _passController.text.trim();
  String get _phone => _phoneController.text.trim();

  @override
  void initViewState(LoginSignUpController controller) {
    super.initViewState(controller);
  }

  @override
  Widget get view {
    //LoginSignUpController controller = new LoginSignUpController(AuthRepositoryImpl());
    return ControlledWidgetBuilder<LoginSignUpController>(
        builder: (context, controller) {
          return Scaffold(
              key: globalKey,
              body: ListView(padding: EdgeInsets.only(top: 100, left: 15, right: 15), children: <Widget>[
                Center(
                  child: Column (
                      children: [
                        const Padding(padding: EdgeInsets.all(5.0)),
                        Image.asset(
                          'assets/logos/bigDarkLogo.png',
                          filterQuality: FilterQuality.high,
                        ),
                        TextField(
                          controller: _userController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            icon: Icon(Icons.person),
                            labelText: 'Username/Email (Use username right now)',
                          ),
                        ),
                        TextField(
                            controller: _emailController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              icon: Icon(Icons.email),
                              labelText: 'E-mail',
                            )
                        ),
                        TextField(
                            controller: _passController,
                            obscureText: true,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              icon: Icon(Icons.lock),
                              labelText: 'Password',
                            )
                        ),
                        TextField(
                            controller: _phoneController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              icon: Icon(Icons.phone),
                              labelText: 'Cell Number',
                            )
                        ),
                        RaisedButton(
                            onPressed: () => controller.signUp(_getSignupInfo()),
                            child: const Text('Sign Up')
                        ),
                      ]
                  ),
                )
              ])
          );
        }
    );
  }

  SignupInfo _getSignupInfo() {
    return new SignupInfo(
        username: _user, email: _email, password: _password, phone: _phone);
  }

}
