
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:pedal_controller/app/pages/login_signup/login_signup_controller.dart';
import 'package:pedal_controller/app/pages/login_signup/signup_page.dart';
import 'package:pedal_controller/domain/repositories/auth_repository_impl.dart';

import 'confirm_signup_page.dart';

class LoginSignUpPage extends View {
  @override
  State<StatefulWidget> createState() => LoginSignUpState();
}

class LoginSignUpState
    extends ViewState<LoginSignUpPage, LoginSignUpController> {
  TextEditingController _userController = TextEditingController();
  TextEditingController _passController = TextEditingController();

  LoginSignUpState() : super(LoginSignUpController(AuthRepositoryImpl()));

  String get user => _userController.text.trim();
  String get password => _passController.text.trim();

  @override
  void initViewState(LoginSignUpController controller) {
    super.initViewState(controller);
  }

  @override
  Widget get view {
    return ControlledWidgetBuilder<LoginSignUpController>(
        builder: (context, controller) {
          return Scaffold(
              key: globalKey,
              body: ListView(padding: EdgeInsets.only(top: 100, left: 15, right: 15), children: <Widget>[
                Center(
                  child: Column (
                      children: [
                        const Padding(padding: EdgeInsets.all(5.0)),
                        Image.asset(
                          'assets/logos/bigDarkLogo.png',
                          filterQuality: FilterQuality.high,
                        ),
                        TextField(
                          controller: _userController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            icon: Icon(Icons.person),
                            labelText: 'Username/Email (Use username right now)',
                          ),
                        ),
                        TextField(
                            controller: _passController,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              icon: Icon(Icons.lock),
                              labelText: 'Password',
                            )
                        ),
                        RaisedButton(
                            onPressed: () => controller.login(_getLoginData()),
                            child: const Text('Login')
                        ),
                        RaisedButton(
                            onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (context) => SignupPage())),
                            child: const Text('Sign Up')
                        ),
                        RaisedButton(
                            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmSignupPage())),
                            child: const Text('Confirm Sign up')
                        )
                      ]
                  ),
                )
              ])
          );
    });
  }
  
  LoginData _getLoginData() {
    return new LoginData(name: user, password: password);
  }


}

