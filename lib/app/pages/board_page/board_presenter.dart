import 'package:flutter/foundation.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_board_config_usecase.dart';

class BoardPresenter extends Presenter {
  Function getBoardConfigOnNext;
  Function getBoardConfigOnComplete;
  Function getBoardConfigOnError;

  final GetBoardConfigUseCase _getBoardConfigUseCase;

  BoardPresenter(boardConfigRepo, authRepo)
      : _getBoardConfigUseCase = GetBoardConfigUseCase(boardConfigRepo);

  void getBoardConfig({@required User user}) {
    _getBoardConfigUseCase.execute(
        _GetBoardConfigObserver(this), GetBoardConfigUseCaseParams(user));
  }

  @override
  void dispose() {
    _getBoardConfigUseCase.dispose();
  }
}

class _GetBoardConfigObserver
    implements Observer<GetBoardConfigUseCaseResponse> {
  final BoardPresenter _presenter;
  _GetBoardConfigObserver(this._presenter);

  @override
  void onNext(response) {
    this._presenter.getBoardConfigOnNext(response.banks);
  }

  @override
  void onComplete() {
    this._presenter.getBoardConfigOnComplete();
  }

  @override
  void onError(e) {
    this._presenter.getBoardConfigOnError(e);
  }
}
