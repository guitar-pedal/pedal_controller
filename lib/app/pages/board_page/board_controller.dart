import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/board_page/board_presenter.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';

class BoardController extends Controller {
  BoardPresenter _presenter;
  bool _boardConfigRetrieved;
  bool _isLoading;
  List<BankInfo> banks = [];
  final logger;

  BoardController(pedalConfigRepository, authRepository)
      : _presenter = BoardPresenter(pedalConfigRepository, authRepository),
        logger = Logger("BoardController") {
    _boardConfigRetrieved = false;
    _isLoading = true;
    retrieveData();
  }

  void listOnTap(BankInfo info) {
    Navigator.of(getContext()).pushNamed(
      '/bank',
      arguments: info,
    );
  }

  @override
  void initListeners() {
    _presenter.getBoardConfigOnNext = (List<BankInfo> banks) {
      this.banks = banks;
    };
    _presenter.getBoardConfigOnError = (e) {
      dismissLoading();
      this.logger.severe(e);
    };
    _presenter.getBoardConfigOnComplete = () {
      _boardConfigRetrieved = true;
      dismissLoading();
    };
  }

  void dismissLoading() {
    _isLoading = false;
    refreshUI();
  }

  void retrieveData() {
    // TODO: Actual user stuff?
    logger.fine("Retrieving data from presenter");
    _presenter.getBoardConfig(user: null);
  }

  @override
  void dispose() {
    _presenter.dispose();
    super.dispose();
  }

  @override
  void didPopNext() {
    retrieveData();
    super.didPopNext();
  }
}
