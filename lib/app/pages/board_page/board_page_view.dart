import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/board_page/board_controller.dart';
import 'package:pedal_controller/app/widgets/bank_info_card.dart';
import 'package:pedal_controller/app/widgets/drawer/app_drawer.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_auth_repository.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_board_repository.dart';

class BoardPage extends View {
  BoardPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  State createState() => BoardPageView();
}

class BoardPageView extends ViewState<BoardPage, BoardController> {
  BoardPageView()
      : super(BoardController(DummyBoardRepository(), DummyAuthRepository()));

  @override
  Widget get view {
    return Scaffold(
      endDrawer: AppDrawer(),
      key: globalKey,
      appBar: appBar,
      body: ControlledWidgetBuilder<BoardController>(
        builder: (context, controller) {
          List<BankInfoCard> cards =
              controller.banks.map((bank) => BankInfoCard(bank)).toList();
          return ListView(scrollDirection: Axis.vertical, children: cards);
        },
      ),
    );
  }

  AppBar get appBar => AppBar(
        title: Text(
          'My Board',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        centerTitle: true,
      );
}
