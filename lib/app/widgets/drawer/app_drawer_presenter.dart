import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/usecases/get_current_user_info_usecase.dart';

class AppDrawerPresenter extends Presenter {
  Function getUserInfoOnNext;
  Function getUserInfoOnComplete;
  Function getUserInfoOnError;

  final GetCurrentUserInfoUseCase _useCase;

  AppDrawerPresenter(authRepo) : _useCase = GetCurrentUserInfoUseCase(authRepo);

  void getCurrentUser() {
    _useCase.execute(
      _GetCurrentUserObserver(this),
      GetCurrentUserInfoParams("foo", "bar"),
    );
  }

  @override
  void dispose() {
    _useCase.dispose();
  }
}

class _GetCurrentUserObserver extends Observer<GetCurrentUserInfoResponse> {
  final AppDrawerPresenter _presenter;

  _GetCurrentUserObserver(this._presenter);

  @override
  void onError(e) {
    _presenter.getUserInfoOnError(e);
  }

  @override
  void onComplete() {
    _presenter.getUserInfoOnComplete();
  }

  @override
  void onNext(GetCurrentUserInfoResponse response) {
    _presenter.getUserInfoOnNext(response.user);
  }
}
