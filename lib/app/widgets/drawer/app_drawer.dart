import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/widgets/drawer/app_drawer_controller.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_auth_repository.dart';

class AppDrawer extends View {
  AppDrawer({Key key}) : super(key: key);

  @override
  AppDrawerViewState createState() => AppDrawerViewState();
}

class AppDrawerViewState extends ViewState<AppDrawer, AppDrawerController> {
  AppDrawerViewState() : super(AppDrawerController(DummyAuthRepository()));

  @override
  Widget get view {
    return Drawer(
      key: globalKey,
      child: ControlledWidgetBuilder<AppDrawerController>(
          builder: (context, controller) {
        return ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              // TODO: Get user's information from a repo or something
              accountEmail: Text(controller.currentUser.email),
              accountName: Text(controller.currentUser.fullName),
              currentAccountPicture: CircleAvatar(
                child: Text(controller.currentUser.initials),
              ),
            ),
            ListTile(
              leading: Icon(Icons.developer_board),
              title: Text("Board"),
              onTap: () {
                controller.drawerListTileOnTap("Board");
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text("Settings"),
              onTap: () {
                // TODO: Navigate to settings page
                controller.drawerListTileOnTap("Settings");
              },
            ),
            ListTile(
              leading: Icon(Icons.info),
              title: Text("App Info"),
              onTap: () {
                // TODO: Display the AboutDialog with app version info etc
                controller.drawerListTileOnTap("App Info");
              },
            )
          ],
        );
      }),
    );
  }
}
