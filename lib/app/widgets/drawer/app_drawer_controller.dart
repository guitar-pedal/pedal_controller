import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/widgets/drawer/app_drawer_presenter.dart';
import 'package:pedal_controller/domain/entities/user.dart';

class AppDrawerController extends Controller {
  AppDrawerPresenter _presenter;
  bool _userInfoRetrieved;
  bool _isLoading;
  User currentUser;
  final logger;

  AppDrawerController(authRepo)
      : _presenter = AppDrawerPresenter(authRepo),
        logger = Logger("AppDrawerController") {
    _userInfoRetrieved = false;
    _isLoading = true;
    retrieveData();
  }

  void retrieveData() {
    _presenter.getCurrentUser();
  }

  void dismissLoading() {
    _isLoading = false;
    refreshUI();
  }

  @override
  void initListeners() {
    _presenter.getUserInfoOnNext = (User user) {
      this.currentUser = user;
    };

    _presenter.getUserInfoOnComplete = () {
      this._userInfoRetrieved = true;
      dismissLoading();
    };

    _presenter.getUserInfoOnError = (e) {
      dismissLoading();
      logger.severe(e);
    };
  }

  void drawerListTileOnTap(String title) {
    switch (title) {
      case "Board":
        {
          // Navigate to board view
          logger.fine("/board clicked from drawer");
          Navigator.of(getContext()).pushReplacementNamed('/board');
        }
        break;
      case "Settings":
        {
          // TODO: Navigate to settings view
          logger.fine("/settings clicked from drawer");
        }
        break;
      case "App Info":
        {
          // TODO: Bring up AboutDialog with app information on it
          logger.fine("/appinfo clicked from drawer");
        }
        break;
      default:
        {
          logger.severe("default case, should never happen");
        }
        break;
    }
  }

  @override
  void didPopNext() {
    retrieveData();
    super.didPopNext();
  }
}
