import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/channel_page/channel_page_controller.dart';

class EffectCard extends StatelessWidget {
  final String _name;

  EffectCard(this._name);

  @override
  Widget build(BuildContext context) {
    ChannelPageController controller =
        FlutterCleanArchitecture.getController<ChannelPageController>(context);
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: InkWell(
          onTap: () {
            controller.listOnTap(_name);
          },
          child: SizedBox(
            height: 20.0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[Text(_name)],
            ),
          ),
        ),
      ),
    );
  }
}
