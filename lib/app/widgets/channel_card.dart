import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/bank_page/bank_page_controller.dart';

class ChannelCard extends StatelessWidget {
  final String _name;

  ChannelCard(this._name);

  @override
  Widget build(BuildContext context) {
    BankPageController controller =
        FlutterCleanArchitecture.getController<BankPageController>(context);
    return Card(
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: InkWell(
          onTap: () {
            // FIXME: This will be named channel card when things are renamed
            // TODO: Navigate to appropriate channel view from bank view
            controller.listOnTap(_name);
          },
          child: SizedBox(
            height: 20.0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[Text(_name)],
            ),
          ),
        ),
      ),
    );
  }
}
