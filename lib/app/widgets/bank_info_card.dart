import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/app/pages/board_page/board_controller.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';

class BankInfoCard extends StatelessWidget {
  final BankInfo _info;

  BankInfoCard(this._info);

  @override
  Widget build(BuildContext context) {
    BoardController controller =
        FlutterCleanArchitecture.getController<BoardController>(context);
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: InkWell(
          onTap: () {
            controller.listOnTap(this._info);
          },
          child: SizedBox(
            height: 30.0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  color: Colors.transparent,
                  child: Container(
                    decoration: BoxDecoration(
                        color: this._info.color,
                        borderRadius: BorderRadius.circular(7.0)),
                    alignment: Alignment.center,
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Text(
                      "${this._info.number}",
                      style: TextStyle(fontSize: 27),
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(
                    this._info.name,
                    style: TextStyle(fontSize: 27),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
