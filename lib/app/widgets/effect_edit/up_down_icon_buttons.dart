import 'package:flutter/material.dart';

class UpDownIconButtons extends StatelessWidget {
  final Function decrementOnPush;
  final Function incrementOnPush;

  UpDownIconButtons(
      {@required Function decrementOnPush, @required Function incrementOnPush})
      : this.decrementOnPush = decrementOnPush,
        this.incrementOnPush = incrementOnPush;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
          // Down chevron
          icon: Icon(Icons.expand_more),
          onPressed: decrementOnPush,
        ),
        IconButton(
          // Up chevron
          icon: Icon(Icons.expand_less),
          onPressed: incrementOnPush,
        ),
      ],
    );
  }
}
