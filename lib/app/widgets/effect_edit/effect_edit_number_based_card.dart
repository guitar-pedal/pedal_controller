import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/effect_page/effect_page_controller.dart';
import 'package:pedal_controller/app/widgets/effect_edit/effect_edit_card.dart';
import 'package:pedal_controller/app/widgets/effect_edit/up_down_icon_buttons.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/effect_type.dart';

class EffectEditNumberBasedCard extends StatefulWidget {
  final String index;
  final String title;
  final int startingValue;

  EffectEditNumberBasedCard(this.index, this.title, this.startingValue);

  @override
  _EffectEditNumberBasedCardState createState() =>
      _EffectEditNumberBasedCardState(this.startingValue);
}

class _EffectEditNumberBasedCardState extends State<EffectEditNumberBasedCard>
    implements EffectEditCard {
  int currentValue;

  final TextEditingController _textEditingController;
  final logger;

  _EffectEditNumberBasedCardState(int startingValue)
      : currentValue = startingValue,
        _textEditingController =
            TextEditingController(text: startingValue.toString()),
        logger = Logger("_EffectEditNumberBasedCardState");

  @override
  void initState() {
    super.initState();
    _textEditingController.addListener(setCurrentValue);
    _textEditingController.addListener(persistCurrentValue);
  }

  @override
  Widget build(BuildContext context) {
    return EffectEditCard.buildDefaultEffectEditCard(
        context: context,
        index: this.widget.index,
        title: this.widget.title,
        buildParameterEditors: this.buildParameterEditor);
  }

  Row buildParameterEditor() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        UpDownIconButtons(
          incrementOnPush: this.incrementOnPush,
          decrementOnPush: this.decrementOnPush,
        ),
        Container(
          margin: EdgeInsets.only(right: 20),
          child: SizedBox(
            width: 50,
            child: TextField(
              controller: _textEditingController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              textAlign: TextAlign.center,
            ),
          ),
        )
      ],
    );
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  void setCurrentValue() {
    int newValue = int.parse(this._textEditingController.text);
    // Don't allow change if the input is outside the 0-100 range
    if (inputIsValid(this._textEditingController.text)) {
      setState(() {
        this.currentValue = newValue;
      });
    } else {
      logger.severe("Invalid option.");
      setState(() {
        this._textEditingController.text = this.currentValue.toString();
      });
    }
  }

  void persistCurrentValue() {
    if (inputIsValid(this._textEditingController.text)) {
      EffectPageController controller =
          FlutterCleanArchitecture.getController(context, listen: false);
      controller.setData(EffectEditInfo(this.widget.index, this.widget.title,
          EffectType.number, [], int.parse(this._textEditingController.text)));
      logger.fine("Persisting value: ${this._textEditingController.text}");
    }
  }

  bool inputIsValid(dynamic input) {
    input = int.parse(input);
    return input >= 0 && input <= 100;
  }

  void incrementOnPush() {
    if (this.currentValue < 100) {
      setState(() {
        this.currentValue = this.currentValue + 1;
        this._textEditingController.text = this.currentValue.toString();
      });
    }
  }

  void decrementOnPush() {
    if (this.currentValue > 0) {
      setState(() {
        this.currentValue = this.currentValue - 1;
        this._textEditingController.text = this.currentValue.toString();
      });
    }
  }
}
