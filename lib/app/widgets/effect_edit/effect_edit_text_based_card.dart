import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/app/pages/effect_page/effect_page_controller.dart';
import 'package:pedal_controller/app/widgets/effect_edit/effect_edit_card.dart';
import 'package:pedal_controller/app/widgets/effect_edit/up_down_icon_buttons.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/effect_type.dart';

class EffectEditTextBasedCard extends StatefulWidget {
  final String index;
  final String title;
  final String startingValue;
  final List<String> choices;

  EffectEditTextBasedCard(
      this.index, this.title, this.startingValue, this.choices);

  @override
  _EffectEditTextBasedCardState createState() =>
      _EffectEditTextBasedCardState(this.startingValue);
}

class _EffectEditTextBasedCardState extends State<EffectEditTextBasedCard>
    implements EffectEditCard {
  String dropdownValue;
  final logger;

  _EffectEditTextBasedCardState(String startingValue)
      : dropdownValue = startingValue,
        logger = Logger("_EffectEditTextBasedCardState");

  @override
  Widget build(BuildContext context) {
    return EffectEditCard.buildDefaultEffectEditCard(
        context: context,
        index: this.widget.index,
        title: this.widget.title,
        buildParameterEditors: this.buildParameterEditor);
  }

  Row buildParameterEditor() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        UpDownIconButtons(
          decrementOnPush: this.decrementOnPush,
          incrementOnPush: this.incrementOnPush,
        ),
        dropdown,
      ],
    );
  }

  Container get dropdown {
    return Container(
      margin: EdgeInsets.only(right: 20),
      child: SizedBox(
        width: 50,
        child: DropdownButton<String>(
          isExpanded: true,
          value: dropdownValue,
          icon: Icon(Icons.arrow_drop_down),
          iconSize: 10.0,
          elevation: 10,
          onChanged: onChangedCallback,
          items:
              this.widget.choices.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(
                value,
                overflow: TextOverflow.ellipsis,
              ),
            );
          }).toList(),
        ),
      ),
    );
  }

  void onChangedCallback(String value) {
    if (inputIsValid(value)) {
      setState(() {
        this.dropdownValue = value;
      });
      this.persistCurrentValue();
    }
  }

  void decrementOnPush() {
    // Go to previous item in the list of dropdown
    int i = choices.indexOf(this.dropdownValue);
    i = (i == 0) ? choices.length - 1 : i - 1;
    setState(() {
      this.dropdownValue = choices.elementAt(i);
    });
    this.persistCurrentValue();
  }

  void incrementOnPush() {
    // Go to next item in the list of dropdown
    int i = choices.indexOf(this.dropdownValue);
    i = (i + 1 == choices.length) ? 0 : i + 1;
    setState(() {
      this.dropdownValue = choices.elementAt(i);
    });
    this.persistCurrentValue();
  }

  bool inputIsValid(dynamic input) {
    return this.widget.choices.contains(input);
  }

  void persistCurrentValue() {
    EffectPageController controller =
        FlutterCleanArchitecture.getController(context, listen: false);
    controller.setData(EffectEditInfo(this.widget.index, this.widget.title,
        EffectType.text, this.widget.choices, this.dropdownValue));
    logger.fine("Persisting ${this.dropdownValue}");
  }

  List<String> get choices => this.widget.choices;
}
