import 'package:flutter/material.dart';
import 'package:pedal_controller/app/widgets/effect_edit/effect_edit_number_based_card.dart';
import 'package:pedal_controller/app/widgets/effect_edit/effect_edit_text_based_card.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/effect_type.dart';

class EffectEditCardFactory {
  static Widget createEffectEditCard(EffectEditInfo info) {
    switch (info.type) {
      case EffectType.number:
        return EffectEditNumberBasedCard(info.index, info.name, info.currentVal);
        break;
      case EffectType.text:
        return EffectEditTextBasedCard(
            info.index, info.name, info.currentVal, info.options);
        break;
      default:
        throw UnsupportedError("Invalid choice for creating EffectEditCard");
    }
  }
}
