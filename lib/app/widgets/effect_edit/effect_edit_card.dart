import 'package:flutter/material.dart';

abstract class EffectEditCard {
  void persistCurrentValue();
  bool inputIsValid(dynamic input);
  void incrementOnPush();
  void decrementOnPush();
  Row buildParameterEditor();

  /// Builds a card for an effect-edit allowing users to pass in a [buildParameterEditors]
  /// function which describes
  static Card buildDefaultEffectEditCard(
      {@required BuildContext context,
      @required String index,
      @required String title,
      @required Function buildParameterEditors}) {
    return Card(
      key: ValueKey(index),
      elevation: 5,
      margin: EdgeInsets.only(left: 8, right: 8, bottom: 8, top: 8),
      clipBehavior: Clip.antiAlias,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              // TODO: make nicer
              Container(
                child: Text(index),
                // margin: EdgeInsets.only(left: 8),
              ),
              Container(child: Text(title)),
              buildParameterEditors(),
            ],
          ),
        ),
      ),
    );
  }
}
