import 'package:flutter/foundation.dart';

class SignupInfo {
  String username;
  String email;
  String password;
  String phone;

  SignupInfo({
    @required this.username,
    @required this.email,
    @required this.password,
    this.phone});
}