import 'package:flutter/foundation.dart';

class ConfirmSignupInfo {
  String username;
  String code;

  ConfirmSignupInfo({
    @required this.username,
    @required this.code,
  });
}