import 'package:pedal_controller/domain/entities/effect_type.dart';

class EffectEditInfo {
  final String index;
  final String name;
  final EffectType type;
  final List<String> options;
  final dynamic currentVal;

  EffectEditInfo(this.index, this.name, this.type, this.options, this.currentVal);
}
