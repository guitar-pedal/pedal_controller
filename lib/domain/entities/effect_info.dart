import 'package:pedal_controller/domain/entities/channel_info.dart';

class EffectInfo {
  String name;
  ChannelInfo channelInfo;

  EffectInfo(this.name, this.channelInfo);
}
