import 'package:flutter/material.dart';

class BankInfo {
  final String name;
  final Color color;
  final int number;

  BankInfo(this.number, this.name, this.color);
}
