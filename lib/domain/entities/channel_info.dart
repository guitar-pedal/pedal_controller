import 'package:pedal_controller/domain/entities/bank_info.dart';

/// Class to encapsulate the information passed from bank view to channel view.
/// Requires the name as a [String] of the channel, along with the [BankInfo]
/// of the bank that contains the channel.
class ChannelInfo {
  final String name;
  final BankInfo bankInfo;

  ChannelInfo(this.name, this.bankInfo);
}
