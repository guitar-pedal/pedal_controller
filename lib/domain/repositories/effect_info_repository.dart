import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/user.dart';

abstract class EffectRepository {
  /// Retrieves all effects for a given channel given a
  /// [User] and the name of the channel as a [String].
  Future<List<String>> getEffects(
      {@required User user, @required String channel});
}
