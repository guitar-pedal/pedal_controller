import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';

abstract class BoardRepository {
  /// From some repository, fetches the last saved bank info for the pedal
  Future<List<BankInfo>> getAllBankInfo({@required User user});
}
