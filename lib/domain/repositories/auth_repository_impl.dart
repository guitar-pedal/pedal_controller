import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_core/amplify_core.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  @override
  Future<void> forgotPassword(String email) {
    // TODO: implement forgotPassword
    throw UnimplementedError();
  }

  @override
  Future<User> getCurrentUser() {
    // TODO: implement getCurrentUser
    throw UnimplementedError();
  }

  @override
  Future<bool> isAuthenticated({String username, String code}) async {
    try {
      SignUpResult res = await Amplify.Auth.confirmSignUp(
        username: username,
        confirmationCode: code,
      );

      return res.isSignUpComplete;

    } on AuthError catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Future<bool> login({String email, String password}) async {
    try {
      SignInResult res = await Amplify.Auth.signIn(
        username: email,
        password: password,
      );

      return res.isSignedIn;

    } on AuthError catch (e) {
      print(e);
      return false;
    }
  }

  @override
  Future<void> logout() {
    // TODO: implement logout
    throw UnimplementedError();
  }

  @override
  Future<bool> signUp({String username, String email, String password, String phone}) async {
    try {
      Map<String, dynamic> userAttributes = {
        "email": email,
        "phone_number": phone,
      };
      SignUpResult res = await Amplify.Auth.signUp(
          username: username,
          password: password,
          options: CognitoSignUpOptions(
              userAttributes: userAttributes
          )
      );
      return res.isSignUpComplete;
    } on AuthError catch (e) {
      print(e);
      return false;
    }
  }
}