import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/user.dart';

abstract class AuthRepository {
  Future<bool> signUp({
    @required String username,
    @required String email,
    @required String password,
    String phone
  });

  /// Logs in a user using their [email] and [password]
  Future<bool> login({
    @required String email,
    @required String password
  });

  /// Returns whether the [User] is authenticated.
  Future<bool> isAuthenticated({
      @required String username,
      @required String code
  });

  /// Returns the current authenticated [User].
  Future<User> getCurrentUser();

  /// Resets the password of a [User]
  Future<void> forgotPassword(String email);

  /// Logs out the [User]
  Future<void> logout();
}