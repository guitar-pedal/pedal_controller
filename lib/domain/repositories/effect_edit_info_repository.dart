import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';

abstract class EffectEditInfoRepository {
  /// Retrieves all effect configurations for a given effect given a
  /// [User] and the name of the effect as a [String].
  Future<List<EffectEditInfo>> getEffectEditInfo(
      {@required User user, @required String effect});

  Future<EffectEditInfo> setEffectEditInfo(
      {@required User user, @required EffectEditInfo effect});
}
