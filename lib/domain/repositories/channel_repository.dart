import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/user.dart';

abstract class ChannelRepository {
  /// Retrieves all channels for a given bank given a
  /// [User] and the name of the bank as a [String].

  //TODO: Should this be a list of channel info?
  Future<List<String>> getChannels(
      {@required User user, @required String bank});
}