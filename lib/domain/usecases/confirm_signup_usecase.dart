import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';

class ConfirmSignupUseCase extends CompletableUseCase<ConfirmSignupUseCaseParams>{
  AuthRepository _authRepository;

  ConfirmSignupUseCase(this._authRepository);

  @override
  Future<Stream<ConfirmSignupUseCaseResponse>> buildUseCaseStream(
      ConfirmSignupUseCaseParams params) async {
    final StreamController<ConfirmSignupUseCaseResponse> controller =
    StreamController();
    try {
      bool confirmSignupComplete = await _authRepository.isAuthenticated(
          username: params._username, code: params._code);
      controller.add(ConfirmSignupUseCaseResponse(confirmSignupComplete));
      controller.close();
    } catch (e) {
      //logger.severe('RegisterUseCase unsuccessful.', e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

class ConfirmSignupUseCaseParams {
  String _username;
  String _code;

  ConfirmSignupUseCaseParams(this._username, this._code);
}

class ConfirmSignupUseCaseResponse {
  bool _confirmSignUpComplete;

  bool get confirmSignUpComplete => _confirmSignUpComplete;

  ConfirmSignupUseCaseResponse(this._confirmSignUpComplete);
}