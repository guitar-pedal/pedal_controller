import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/effect_edit_info_repository.dart';

class GetEffectEditInfoUseCase extends UseCase<GetEffectEditInfoUseCaseResponse,
    GetEffectEditInfoUseCaseParams> {
  EffectEditInfoRepository _repository;
  GetEffectEditInfoUseCase(this._repository);

  @override
  Future<Stream<GetEffectEditInfoUseCaseResponse>> buildUseCaseStream(
      GetEffectEditInfoUseCaseParams params) async {
    final StreamController<GetEffectEditInfoUseCaseResponse> controller =
        StreamController<GetEffectEditInfoUseCaseResponse>();
    try {
      final effectEditInfo = await this
          ._repository
          .getEffectEditInfo(user: params.user, effect: params.effectName);
      controller.add(
          GetEffectEditInfoUseCaseResponse(params.effectName, effectEditInfo));
      controller.close();
    } catch (e) {
      logger.severe(e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetEffectEditInfoUseCaseParams {
  User _user;
  String _effectName;

  User get user => _user;

  String get effectName => _effectName;

  GetEffectEditInfoUseCaseParams(this._user, this._effectName);
}

class GetEffectEditInfoUseCaseResponse {
  final String effectName;
  final List<EffectEditInfo> effectEditInfo;
  GetEffectEditInfoUseCaseResponse(this.effectName, this.effectEditInfo);
}
