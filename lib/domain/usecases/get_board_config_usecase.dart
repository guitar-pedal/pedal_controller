import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/board_repository.dart';

/// Retrieves the board config for a [User] from the [_boardConfigRepository].
class GetBoardConfigUseCase extends UseCase<GetBoardConfigUseCaseResponse,
    GetBoardConfigUseCaseParams> {
  BoardRepository _boardConfigRepository;
  GetBoardConfigUseCase(this._boardConfigRepository);

  @override
  Future<Stream<GetBoardConfigUseCaseResponse>> buildUseCaseStream(
      GetBoardConfigUseCaseParams params) async {
    final StreamController<GetBoardConfigUseCaseResponse> controller =
        StreamController<GetBoardConfigUseCaseResponse>();
    try {
      List<BankInfo> banks =
          await _boardConfigRepository.getAllBankInfo(user: params.user);
      controller.add(GetBoardConfigUseCaseResponse(banks));
      controller.close();
    } catch (e) {
      logger.severe(e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetBoardConfigUseCaseParams {
  User _user;
  User get user => _user;

  GetBoardConfigUseCaseParams(this._user);
}

class GetBoardConfigUseCaseResponse {
  final List<BankInfo> banks;
  GetBoardConfigUseCaseResponse(this.banks);
}
