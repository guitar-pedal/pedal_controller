import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/channel_repository.dart';

class GetChannelsUseCase
    extends UseCase<GetChannelsUseCaseResponse, GetChannelsUseCaseParams> {
  ChannelRepository _repository;
  GetChannelsUseCase(this._repository);

  @override
  Future<Stream<GetChannelsUseCaseResponse>> buildUseCaseStream(
      GetChannelsUseCaseParams params) async {
    final StreamController<GetChannelsUseCaseResponse> controller =
        StreamController<GetChannelsUseCaseResponse>();
    try {
      List<String> channels = await this
          ._repository
          .getChannels(user: params.user, bank: params.bankName);
      controller.add(GetChannelsUseCaseResponse(params.bankName, channels));
      controller.close();
    } catch (e) {
      logger.severe(e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetChannelsUseCaseParams {
  User _user;
  String _bankName;

  User get user => _user;
  String get bankName => _bankName;

  GetChannelsUseCaseParams(this._user, this._bankName);
}

class GetChannelsUseCaseResponse {
  final String bankName;
  final List<String> channels;
  GetChannelsUseCaseResponse(this.bankName, this.channels);
}
