import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/effect_edit_info_repository.dart';

class SetEffectEditInfoUseCase extends UseCase<SetEffectEditInfoUseCaseResponse,
    SetEffectEditInfoUseCaseParams> {
  EffectEditInfoRepository _repository;
  SetEffectEditInfoUseCase(this._repository);

  @override
  Future<Stream<SetEffectEditInfoUseCaseResponse>> buildUseCaseStream(
      SetEffectEditInfoUseCaseParams params) async {
    final StreamController<SetEffectEditInfoUseCaseResponse> controller =
        StreamController<SetEffectEditInfoUseCaseResponse>();
    try {
      final effectEditInfo = await this
          ._repository
          .setEffectEditInfo(user: params.user, effect: params.effectEditInfo);
      controller.add(SetEffectEditInfoUseCaseResponse(
          params.effectEditInfo.name, effectEditInfo));
      controller.close();
    } catch (e) {
      logger.severe(e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

class SetEffectEditInfoUseCaseParams {
  User _user;
  EffectEditInfo _effectEditInfo;

  User get user => _user;

  EffectEditInfo get effectEditInfo => _effectEditInfo;

  SetEffectEditInfoUseCaseParams(this._user, this._effectEditInfo);
}

class SetEffectEditInfoUseCaseResponse {
  final String effectName;
  final EffectEditInfo effectEditInfo;
  SetEffectEditInfoUseCaseResponse(this.effectName, this.effectEditInfo);
}
