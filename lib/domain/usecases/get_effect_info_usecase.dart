import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/effect_info_repository.dart';

class GetEffectInfoUseCase
    extends UseCase<GetEffectInfoUseCaseResponse, GetEffectInfoUseCaseParams> {
  EffectRepository _repository;
  GetEffectInfoUseCase(this._repository);

  @override
  Future<Stream<GetEffectInfoUseCaseResponse>> buildUseCaseStream(
      GetEffectInfoUseCaseParams params) async {
    final StreamController<GetEffectInfoUseCaseResponse> controller =
        StreamController<GetEffectInfoUseCaseResponse>();
    try {
      final effectInfo = await this
          ._repository
          .getEffects(user: params.user, channel: params.channel);
      controller.add(GetEffectInfoUseCaseResponse(params.channel, effectInfo));
      controller.close();
    } catch (e) {
      logger.severe(e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

class GetEffectInfoUseCaseParams {
  User _user;
  String _channel;

  User get user => _user;

  String get channel => _channel;

  GetEffectInfoUseCaseParams(this._user, this._channel);
}

class GetEffectInfoUseCaseResponse {
  final String channel;
  // TODO: Encapsulate the effects into an entity?
  final List<String> effects;
  GetEffectInfoUseCaseResponse(this.channel, this.effects);
}
