import 'dart:async';
import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';

class LoginUseCase extends CompletableUseCase<LoginUseCaseParams> {
  AuthRepository _authRepository;
  LoginUseCase(this._authRepository) : super();

  @override
  Future<Stream<LoginUseCaseResponse>> buildUseCaseStream(LoginUseCaseParams params) async {
    final StreamController<LoginUseCaseResponse> controller = StreamController();
    try {
      bool loginComplete = await _authRepository.login(
          email: params._email, password: params._password);
      controller.add(LoginUseCaseResponse(loginComplete));
      controller.close();
    } catch (e) {
      //_logger.shout('Could not login the user.', e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

/// The parameters required for the [LoginUseCase]
class LoginUseCaseParams {
  String _email;
  String _password;

  LoginUseCaseParams(this._email, this._password);
}

class LoginUseCaseResponse {
  bool _loginComplete;

  bool get loginComplete => _loginComplete;

  LoginUseCaseResponse(this._loginComplete);
}