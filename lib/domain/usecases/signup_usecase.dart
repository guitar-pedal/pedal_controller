import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';

/// A `UseCase` for registering a new `User` in the application
class SignUpUseCase extends CompletableUseCase<SignUpUseCaseParams> {
  // Members
  AuthRepository _authRepository;

  // Constructors
  SignUpUseCase(this._authRepository);

  @override
  Future<Stream<SignUpUseCaseResponse>> buildUseCaseStream(
      SignUpUseCaseParams params) async {
    final StreamController<SignUpUseCaseResponse> controller =
        StreamController();
    try {
      bool signUpComplete = await _authRepository.signUp(
          username: params._username, email: params._email, password: params._password, phone: params._phone);
      controller.add(SignUpUseCaseResponse(signUpComplete));
      controller.close();
    } catch (e) {
      //logger.severe('RegisterUseCase unsuccessful.', e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

/// The parameters required for the [SignUpUseCase]
class SignUpUseCaseParams {
  String _username;
  String _email;
  String _password;
  String _phone;

  SignUpUseCaseParams(this._username, this._email, this._password, this._phone);
}

class SignUpUseCaseResponse {
  bool _signUpComplete;

  bool get signUpComplete => _signUpComplete;

  SignUpUseCaseResponse(this._signUpComplete);
}
