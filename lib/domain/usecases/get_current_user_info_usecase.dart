import 'dart:async';

import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';

class GetCurrentUserInfoUseCase
    extends CompletableUseCase<GetCurrentUserInfoParams> {
  AuthRepository _authRepository;
  GetCurrentUserInfoUseCase(this._authRepository) : super();

  @override
  Future<Stream<GetCurrentUserInfoResponse>> buildUseCaseStream(
      GetCurrentUserInfoParams params) async {
    final StreamController<GetCurrentUserInfoResponse> controller =
        StreamController();
    try {
      User currentUser = await _authRepository.getCurrentUser();
      controller.add(GetCurrentUserInfoResponse(currentUser));
      controller.close();
    } catch (e) {
      //_logger.shout('Could not login the user.', e);
      controller.addError(e);
    }
    return controller.stream;
  }
}

/// The parameters required for the [LoginUseCase]
class GetCurrentUserInfoParams {
  // TODO: What are the params gonna be?
  String _email;
  String _password;

  GetCurrentUserInfoParams(this._email, this._password);
}

class GetCurrentUserInfoResponse {
  User _user;

  User get user => _user;

  GetCurrentUserInfoResponse(this._user);
}
