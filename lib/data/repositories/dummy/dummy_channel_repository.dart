import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/channel_repository.dart';

class DummyChannelRepository implements ChannelRepository {
  Future<List<String>> getChannels(
      {@required User user, @required String bank}) async {
    List<String> channels = new List<String>();
    channels.add("Vibrato");
    channels.add("Chorus");
    channels.add("Tremolo");
    channels.add("Flanger");
    channels.add("Phaser");
    channels.add("Rotary");
    channels.add("Chorus 2");
    return channels;
  }
}
