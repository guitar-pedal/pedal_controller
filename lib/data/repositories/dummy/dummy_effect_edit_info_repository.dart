import 'package:flutter/cupertino.dart';
import 'package:logging/logging.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/effect_type.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/effect_edit_info_repository.dart';

class DummyEffectEditInfoRepository implements EffectEditInfoRepository {
  final logger = Logger("DummyEffectEditInfoRepository");
  @override
  Future<List<EffectEditInfo>> getEffectEditInfo(
      {@required User user, @required String effect}) async {
    return [
      EffectEditInfo(
        "A",
        "Type",
        EffectType.text,
        ["Hall", "Garage", "Cave"],
        "Hall",
      ),
      EffectEditInfo(
        "B",
        "Level",
        EffectType.number,
        null,
        20,
      ),
      EffectEditInfo(
        "C",
        "Rate",
        EffectType.number,
        null,
        66,
      ),
      EffectEditInfo(
        "X",
        "Depth",
        EffectType.number,
        null,
        40,
      ),
      EffectEditInfo(
        "Y",
        "Tone",
        EffectType.number,
        null,
        99,
      ),
      EffectEditInfo(
        "Z",
        "Test",
        EffectType.number,
        null,
        100,
      )
    ];
  }

  @override
  Future<EffectEditInfo> setEffectEditInfo(
      {@required User user, @required EffectEditInfo effect}) async {
    logger.fine("Repository received new params for effect ${effect.name}");
    await Future.delayed(Duration(milliseconds: 500));
    return effect;
  }
}
