import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:pedal_controller/domain/entities/bank_info.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/board_repository.dart';

class DummyBoardRepository implements BoardRepository {
  Future<List<BankInfo>> getAllBankInfo({@required User user}) async {
    List<BankInfo> bankInfo = new List<BankInfo>();
    bankInfo.add(BankInfo(1, "Compressor", Color(0xffff0000)));
    bankInfo.add(BankInfo(2, "Overdrive", Color(0xffff6700)));
    bankInfo.add(BankInfo(3, "Distortion", Color(0xfffff400)));
    bankInfo.add(BankInfo(4, "Modulation", Color(0xff00ff00)));
    bankInfo.add(BankInfo(5, "Delay", Color(0xff0000ff)));
    bankInfo.add(BankInfo(6, "Reverb", Color(0xff6300ff)));
    bankInfo.add(BankInfo(7, "Test 1", Color(0xffe300ff)));
    bankInfo.add(BankInfo(8, "Test 2", Color(0xff815c41)));

    return bankInfo;
  }
}
