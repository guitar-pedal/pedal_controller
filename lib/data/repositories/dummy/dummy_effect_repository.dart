import 'package:flutter/foundation.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/effect_info_repository.dart';

class DummyEffectRepository implements EffectRepository {
  Future<List<String>> getEffects(
      {@required User user, @required String channel}) async {
    List<String> effectInfo = new List<String>();
    effectInfo.add("Chorus");
    effectInfo.add("Delay");
    effectInfo.add("Reverb");

    return effectInfo;
  }
}
