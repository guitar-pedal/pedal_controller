import 'package:flutter/material.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/repositories/auth_repository.dart';

class DummyAuthRepository implements AuthRepository {
  // Members
  /// Singleton object of `DummyAuthenticationRepository`
  static DummyAuthRepository _instance = DummyAuthRepository._internal();

  // Constructors
  DummyAuthRepository._internal();

  factory DummyAuthRepository() => _instance;

  // AuthenticationRepository Methods

  /// Simulates the sign up process for a user. Returns after a short delay
  Future<bool> signUp(
      {@required String username, @required String email, @required String password, String phone}) async {
    return true;
  }

  /// Fake logs in a user. Returns after a short delay
  Future<bool> login(
      {@required String email, @required String password}) async {
    return true;
  }

  /// Returns whether the current `User` is authenticated.
  Future<bool> isAuthenticated(
      {@required String username, @required String code}) async {
    return false;
  }

  Future<void> forgotPassword(String email) async {}

  /// Logs the current `User` out by clearing credentials.
  Future<void> logout() async {
    // TODO: Log out the user
  }

  /// Returns a fake authenticated `User`.
  Future<User> getCurrentUser() async {
    return User('Eric', 'Clapton', 'eric.clapton@galaxysounds.ca',
        'eric.clapton@galaxysounds.ca');
  }
}
