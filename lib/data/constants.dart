library constants;

const String PROJECT_TITLE = "Asteroid Effects Pedal";
const String LANDING_SCREEN_APP_BAR = PROJECT_TITLE;
const String LOADING_SCREEN_APP_BAR = PROJECT_TITLE;
const String CONFIGURING_AMPLIFY_MSG =
    "Please Wait.  Configuring Amplify Flutter SDK";
const String BOARD_VIEW = "Board View";
const String BANK_VIEW = "Bank View";
const String CHANNEL_VIEW = "Channel View";
const String SIGN_IN_TEXT = "Sign In";
const String SIGN_OUT_TEXT = "Sign Out";
const String SIGN_UP_TEXT = "Sign Up";
const String CANCEL_TEXT = "Cancel";
const String USERNAME_HINT = "Enter your username";
const String USERNAME_LABEL = "Username *";
const String EMAIL_HINT = "Enter your email";
const String EMAIL_LABEL = "Email *";
const String PASSWORD_HINT = "Enter your password";
const String PASSWORD_LABEL = "Password *";
const String CONFIRMATION_HINT = "The code we sent you";
const String CONFIRMATION_LABEL = "Confirmation Code *";
