import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_auth_repository.dart';
import 'package:pedal_controller/domain/usecases/get_current_user_info_usecase.dart';
import 'package:test/test.dart';

void main() {
  group('GetCurrentUserInfoUseCase', () {
    GetCurrentUserInfoUseCase useCase;
    _Observer _observer;
    GetCurrentUserInfoParams params;

    setUp(() {
      useCase = GetCurrentUserInfoUseCase(DummyAuthRepository());
      _observer = _Observer();
      params = GetCurrentUserInfoParams(null, null);
    });

    test('.execute() retrieves a valid user', () async {
      useCase.execute(_observer, params);
      while (!_observer.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      useCase.dispose();
    });
  });
}

/// Observer to execute [UseCase] with
class _Observer implements Observer<GetCurrentUserInfoResponse> {
  bool done = false;
  void onNext(response) {
    expect(response is GetCurrentUserInfoResponse, true);
    expect(response.user.fullName, 'Eric Clapton');
    expect(response.user.email, 'eric.clapton@galaxysounds.ca');
    expect(response.user.initials, 'EC');
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}
