import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_channel_repository.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_channels_usecase.dart';
import 'package:test/test.dart';

void main() {
  group('GetChannelsUseCase', () {
    GetChannelsUseCase _getChannelsUseCase;
    _Observer observer;
    User _testUser = new User('test', 'test', 'test@test.com', 'test@test.com');
    GetChannelsUseCaseParams _params;

    setUp(() {
      _getChannelsUseCase = GetChannelsUseCase(DummyChannelRepository());
      observer = _Observer();
      _params = GetChannelsUseCaseParams(_testUser, 'testBank');
    });

    test('.execute() retrieves a list of channels', () async {
      _getChannelsUseCase.execute(observer, _params);
      while (!observer.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      _getChannelsUseCase.dispose();
    });
  });
}

/// Observer to execute [UseCase] with
class _Observer implements Observer<GetChannelsUseCaseResponse> {
  bool done = false;
  void onNext(response) {
    expect(response, TypeMatcher<GetChannelsUseCaseResponse>());
    expect(response.bankName, 'testBank');
    expect(response.channels.length, 7);
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}
