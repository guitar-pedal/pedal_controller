import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_effect_edit_info_repository.dart';
import 'package:pedal_controller/domain/entities/effect_edit_info.dart';
import 'package:pedal_controller/domain/entities/effect_type.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/set_effect_edit_info_usecase.dart';
import 'package:test/test.dart';

void main() {
  group('SetEffectEditInfoUseCase', () {
    SetEffectEditInfoUseCase _setEffectEditInfoUseCase;
    _ObserverNumeric observerNumeric;
    _ObserverText observerText;
    User _testUser;
    EffectEditInfo _editInfoNumber;
    EffectEditInfo _editInfoText;

    setUp(() {
      _setEffectEditInfoUseCase =
          SetEffectEditInfoUseCase(DummyEffectEditInfoRepository());
      observerNumeric = _ObserverNumeric();
      observerText = _ObserverText();
      _editInfoNumber = EffectEditInfo('1', "test", EffectType.number, [], 60);
      _editInfoText = EffectEditInfo(
          '2', 'test', EffectType.text, ['Garage', 'Hall'], 'Garage');
      _testUser = new User('test', 'test', 'test@test.com', 'test@test.com');
    });

    test('.execute() sets the effect edit info for type numeric', () async {
      _setEffectEditInfoUseCase.execute(observerNumeric,
          SetEffectEditInfoUseCaseParams(_testUser, _editInfoNumber));
      while (!observerNumeric.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      _setEffectEditInfoUseCase.dispose();
    });

    test('.execute() sets the effect edit info for type text', () async {
      _setEffectEditInfoUseCase.execute(observerText,
          SetEffectEditInfoUseCaseParams(_testUser, _editInfoText));
      while (!observerText.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      _setEffectEditInfoUseCase.dispose();
    });
  });
}

/// Observer to execute [UseCase] with
class _ObserverNumeric implements Observer<SetEffectEditInfoUseCaseResponse> {
  bool done = false;
  void onNext(response) {
    expect(response, TypeMatcher<SetEffectEditInfoUseCaseResponse>());
    expect(response.effectName, 'test');
    expect(response.effectEditInfo.options, []);
    expect(response.effectEditInfo.index, '1');
    expect(response.effectEditInfo.type, EffectType.number);
    expect(response.effectEditInfo.currentVal, 60);
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}

/// Observer to execute [UseCase] with
class _ObserverText implements Observer<SetEffectEditInfoUseCaseResponse> {
  bool done = false;
  void onNext(response) {
    expect(response, TypeMatcher<SetEffectEditInfoUseCaseResponse>());
    expect(response.effectName, 'test');
    expect(response.effectEditInfo.options, ['Garage', 'Hall']);
    expect(response.effectEditInfo.index, '2');
    expect(response.effectEditInfo.type, EffectType.text);
    expect(response.effectEditInfo.currentVal, 'Garage');
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}
