import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_board_repository.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_board_config_usecase.dart';
import 'package:test/test.dart';

void main() {
  group('GetBoardConfigUseCase', () {
    GetBoardConfigUseCase _getPedalConfigUseCase;
    _Observer observer;
    User _testUser = new User('test', 'test', 'test@test.com', 'test@test.com');
    GetBoardConfigUseCaseParams _params;

    setUp(() {
      _getPedalConfigUseCase =
          GetBoardConfigUseCase(DummyBoardRepository());
      observer = _Observer();
      _params = GetBoardConfigUseCaseParams(_testUser);
    });

    test('.execute() retrieves the board configuration', () async {
      _getPedalConfigUseCase.execute(observer, _params);
      while (!observer.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      _getPedalConfigUseCase.dispose();
    });
  });
}

/// Observer to execute [UseCase] with
class _Observer implements Observer<GetBoardConfigUseCaseResponse> {
  bool done = false;
  void onNext(response) {
    expect(response, TypeMatcher<GetBoardConfigUseCaseResponse>());
    expect(response.banks.length, 8);
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}
