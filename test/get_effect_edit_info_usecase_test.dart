import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_effect_edit_info_repository.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_effect_edit_info_usecase.dart';
import 'package:test/test.dart';

void main() {
  group('GetEffectEditInfoUseCase', () {
    GetEffectEditInfoUseCase _getEffectEditInfoUseCase;
    _Observer observer;
    User _testUser = new User('test', 'test', 'test@test.com', 'test@test.com');
    GetEffectEditInfoUseCaseParams _params;

    setUp(() {
      _getEffectEditInfoUseCase = GetEffectEditInfoUseCase(DummyEffectEditInfoRepository());
      observer = _Observer();
      _params = GetEffectEditInfoUseCaseParams(_testUser, 'testEffect');
    });

    test('.execute() retrieves a list of effect edit info', () async {
      _getEffectEditInfoUseCase.execute(observer, _params);
      while (!observer.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      _getEffectEditInfoUseCase.dispose();
    });
  });
}

/// Observer to execute [UseCase] with
class _Observer implements Observer<GetEffectEditInfoUseCaseResponse> {
  bool done = false;
  void onNext(response) {
    expect(response, TypeMatcher<GetEffectEditInfoUseCaseResponse>());
    expect(response.effectName, 'testEffect');
    expect(response.effectEditInfo.length, 6);
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}
