import 'package:flutter_clean_architecture/flutter_clean_architecture.dart';
import 'package:pedal_controller/data/repositories/dummy/dummy_effect_repository.dart';
import 'package:pedal_controller/domain/entities/user.dart';
import 'package:pedal_controller/domain/usecases/get_effect_info_usecase.dart';
import 'package:test/test.dart';

void main() {
  group('GetEffectsUseCase', () {
    GetEffectInfoUseCase _getEffectInfoUseCase;
    _Observer observer;
    User _testUser = new User('test', 'test', 'test@test.com', 'test@test.com');
    GetEffectInfoUseCaseParams _params;

    setUp(() {
      _getEffectInfoUseCase = GetEffectInfoUseCase(DummyEffectRepository());
      observer = _Observer();
      _params = GetEffectInfoUseCaseParams(_testUser, 'testChannel');
    });

    test('.execute() retrieves a list of effects', () async {
      _getEffectInfoUseCase.execute(observer, _params);
      while (!observer.done) {
        await Future.delayed(Duration(seconds: 1));
      }
      _getEffectInfoUseCase.dispose();
    });
  });
}

/// Observer to execute [UseCase] with
class _Observer implements Observer<GetEffectInfoUseCaseResponse> {
  bool done = false;
  void onNext(response) {
    expect(response, TypeMatcher<GetEffectInfoUseCaseResponse>());
    expect(response.channel, 'testChannel');
    expect(response.effects.length, 3);
    done = true;
  }

  void onComplete() {
    print("Complete");
  }

  void onError(e) {
    print(e);
    throw e;
  }
}
