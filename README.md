# Pedal Controller

This repository contains the code for the application that users use
to communicate with the Asteroid Pedal. It is written in Dart
using Google's Flutter cross-platform development framework.

## Before Getting Started
Before getting started with development on this application, it is important to
at least be familiar with a few things. No need to be an expert on any of these
topics, but a little background knowledge will go a long way.
* [Dart Programming Language](https://dart.dev)
    * Very easy to learn, especially for those proficient in Java.
* [Flutter Framework](https://flutter.dev)
    * It would be especially helpful if you read one of Flutter's guides on
    migrating from another web/mobile framework (Android, iOS, React Native).
    These guides lay out the equivalencies between frameworks really nicely.
* [Uncle Bob's Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
* [Flutter Clean Architecture Package](https://pub.dev/packages/flutter_clean_architecture)
    * This package is what is used throughout this app to help follow Clean Architecture
    principles.

## Getting Started

To get started working on this repository, or to simply build and run
the code from source, follow the following steps (before cloning the
repository).

1. Follow the [Flutter documentation](https://flutter.dev/docs/get-started/install)
 for setting up Flutter on your machine. It is recommended to download the source
 code for Flutter instead of the zip, so you have more freedom with the versions 
 and channels of Flutter that are available. Do all of the instructions for Android,
 iOS, and Web setup (but not the deployment for any of these), as this app
 was developed for all of these (if you are on a Windows machine forget about the 
 iOS stuff). The recommended editor for working withthis project is IntelliJ 
 IDEA / Android Studio. You can stop following the documentation once the editor
 is set up, no need to do the test drive app.
 **IF ANYTHING GOES WRONG ALONG THE WAY, FLUTTER DOCTOR IS YOUR FRIEND, AS IS GOOGLE**
2. Clone this repo, and open the project in your IDE (IntelliJ or Android Studio).
3. When the project loads, using the top bar, choose a run target (an emulator or
Chrome web browser) and build hit run. It should kick off a build of the app,
and open it on your build target. On of the best parts of Flutter is the hot-reload
functionality, where the app will refresh automatically when you change the code,
allowing for fast development loops.

### Setup Issues
Windows Users using IntelliJ or Android Studio may experience some trouble setting up the project
to work with their IDE. The project will not be indexed such that code navigation and syntax-
highlighting will not work. This error will either occur immediately upon cloning the project or
when the user checks out another branch. One good thing to check before troubleshooting further is
to ensure that your IDE has flutter and dart installed and enabled. Navigate to your IDE's
settings > 'Languages and Frameworks' and ensure they are enabled and that their sdk path is given.

While the user will be able to run or emulate the app in this state, there is a simple fix that will
kickstart the IDE to index the project again:

1. Close your IDE
2. Navigate to your project folder in the windows file explorer
3. Navigate to [PROJECT] > .idea >
4. Delete 'modules.xml'
5. Open IDE and your project should begin indexing.
6. When indexing is complete, run 'flutter pub get'.

## Other Resources
* [r/FlutterDev](https://www.reddit.com/r/FlutterDev/) an active subreddit dedicated to all things Flutter.
* [This app](https://github.com/ShadyBoukhary/Axion-Technologies-HnH) is an example of how the framework is used in Flutter (somewhat outdataed
    as of November 2020), but was a good starting point for what exists in this app.
* [Clean Code](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwic7c-X5ufsAhXtlnIEHbo1DyIQFjAAegQIARAC&url=https%3A%2F%2Fwww.amazon.ca%2FClean-Code-Handbook-Software-Craftsmanship%2Fdp%2F0132350882&usg=AOvVaw1nSfC4zQ4EHLUOqsuf2ouZ)
* [Clean Architecture](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwimy_-g5ufsAhUHhXIEHSUbCbEQFjAAegQIARAC&url=https%3A%2F%2Fwww.amazon.ca%2FClean-Architecture-Craftsmans-Software-Structure%2Fdp%2F0134494164&usg=AOvVaw3M2oZi3DDoi1fjysPTDbnb)